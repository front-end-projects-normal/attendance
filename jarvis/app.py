import cv2
from pyzbar import pyzbar
from oauth2client.service_account import ServiceAccountCredentials
import gspread

from flask import Flask, jsonify
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route('/')
def run_program():
    # Your Python program code here
    return jsonify({"message": "Python program executed successfully!"})

if __name__ == '__main__':
    app.run()





# Define the Google Sheet and worksheet name
SHEET_NAME = "Names List"
WORKSHEET_NAME = "Sheet1"

# Define the column indices for the name and access granted columns
NAME_COLUMN_INDEX = 1
ACCESS_GRANTED_COLUMN_INDEX = 2

# Define the Google Sheets credentials file path
CREDENTIALS_FILE_PATH = "creds.json"

# Load the Google Sheets credentials
credentials = ServiceAccountCredentials.from_json_keyfile_name(CREDENTIALS_FILE_PATH, ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive'])

# Authorize the Google Sheets API
gc = gspread.authorize(credentials)

# Open the Google Sheet and worksheet
worksheet = gc.open(SHEET_NAME).worksheet(WORKSHEET_NAME)

# Read the list of names from the Google Sheet
names = worksheet.col_values(NAME_COLUMN_INDEX)

# Initialize the camera
camera = cv2.VideoCapture(0)

while camera.isOpened():
    # Read a frame from the camera
    _, frame = camera.read()
    
    # Find and decode QR codes in the frame
    decoded_objects = pyzbar.decode(frame)
    
    # Loop through the decoded objects
    for obj in decoded_objects:
        # Check if the decoded object is a QR code
        if obj.type == 'QRCODE':
            # Get the name from the QR code data
            name = obj.data.decode('utf-8')
            
            # Check if the name matches a name in the list
            if name in names:
                # Find the row index for the name in the worksheet
                row_index = names.index(name) + 1
                
                # Check if access has already been granted
                if worksheet.cell(row_index, ACCESS_GRANTED_COLUMN_INDEX).value == "Access Granted":
                    print("Access already granted for this name")
                else:
                    # Grant access and update the worksheet
                    worksheet.update_cell(row_index, ACCESS_GRANTED_COLUMN_INDEX, "Access Granted")
                    worksheet.format(f"C{row_index}", {"backgroundColor": {"red": 0.0, "green": 1.0, "blue": 0.0}})
                    print("Access granted for this name")
            else:
                print("Name not found in the list of names")
                
    # Show the frame in a window
    cv2.imshow("QR Scanner", frame)
    
    # Wait for a key press
    key = cv2.waitKey(1)
    if key == ord('q'):
        # Quit if the 'q' key is pressed
        break

# Release the camera and close the window
camera.release()
cv2.destroyAllWindows()
